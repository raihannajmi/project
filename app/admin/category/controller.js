const Category = require("./model");

const createCategory = async (req, res, next) => {
  try {
    let payload = req.body;

    let category = new Category(payload);

    await category.save();

    return res.json(category);
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const updateCategory = async (req, res, next) => {
  try {
    let payload = req.body;

    let category = await Category.findOneAndUpdate(
      { _id: req.params.id },
      payload,
      { new: true, runValidators: true }
    );

    return res.json(category);
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const getCategory = async (req, res, next) => {
  try {
    let { limit = 10, skip = 0, query = "" } = req.query;
    let criteria = {};

    // filter berdasarkan query
    if (query.length) {
      criteria = { ...criteria, name: { $regex: `${query}`, $options: "i" } };
    }

    let count = await Category.find(criteria).countDocuments();
    let category = await Category.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip));

    return res.json({ data: category, count });
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const deleteCategory = async (req, res, next) => {
  try {
    let deleted = await Category.findOneAndDelete({ _id: req.params.id });

    return res.json(deleted);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createCategory,
  updateCategory,
  deleteCategory,
  getCategory,
};
