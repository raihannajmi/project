const mongoose = require("mongoose");

const { model, Schema } = mongoose;

const productSchema = Schema(
  {
    name: {
      type: String,
      minLength: [3, "Panjang nama makanan minimal 3 karakter"],
      maxLength: [255, "Panjang nama makanan maksimal 255 karakter"],
      required: [true, "name is required"],
    },

    description: {
      type: String,
      maxLength: [1000, "panjang deskripsi maksimal 1000 karakter"],
      required: [true, "description is required"],
    },

    price: {
      type: Number,
      required: [true, "price is required"],
    },

    unit: {
      type: Number,
      required: [true, "Unit is required"],
    },

    regularPrice: {
      type: Number,
      required: [true, "regular price is required"],
    },

    image: String,

    weight: { type: Number, required: true },

    size: { type: Number || null, required: true },

    category: [
      {
        type: Schema.Types.ObjectId,
        ref: "Category",
        required: true,
      },
    ],

    location: [
      {
        type: Schema.Types.ObjectId,
        ref: "Location",
        required: true,
      },
    ],
  },
  { timestamps: true }
);

module.exports = model("Product", productSchema, "products");
