const fs = require("fs");
const path = require("path");

const Product = require("./model");
const Category = require("../category/model");
const Location = require("../location/model");
const config = require("../../config");

// endpoint create product
const createProduct = async (req, res, next) => {
  try {
    let payload = req.body;

    if (payload.category && payload.category.length) {
      let category = await Category.findById(payload.category);
      if (category.length) {
        payload = {
          ...payload,
          category: category.map((categoryId) => categoryId._id),
        };
      }
    }

    if (payload.location && payload.location.length) {
      let location = await Location.findById(payload.location);
      if (location.length) {
        payload = {
          ...payload,
          location: location.map((locationId) => locationId._id),
        };
      }
    }

    if (req.file) {
      let tmp_path = req.file.path;

      let originalExt = req.file.originalname.split(".");

      [req.file.originalname.split(".").length - 1];

      let filename = req.file.filename + "." + originalExt;

      let target_path = path.resolve(
        config.rootPath,
        `public/upload/${filename}`
      );

      const src = fs.createReadStream(tmp_path);

      const dest = fs.createWriteStream(target_path);

      src.pipe(dest);

      src.on("end", async () => {
        let product = new Product({ ...payload, image_url: filename });

        await product.save();

        return res.json(product);
      });

      src.on("error", async () => {
        next(err);
      });
    } else {
      let product = new Product(payload);

      await product.save();

      return res.json(product);
    }
  } catch (err) {
    if (err && err.name === "ValidationError") {
      return res.json({
        error: 1,
        message: err.message,
        fields: err.errors,
      });
    }
    next(err);
  }
};

const getListProduct = async (req, res, next) => {
  try {
    let {
      limit = 10,
      skip = 0,
      search = "",
      category = [],
      location = [],
    } = req.query;
    let criteria = {};

    if (search.length) {
      criteria = { ...criteria, name: { $regex: `${search}`, $options: "i" } };
    }

    // filter berdasarkan category
    if (category.length) {
      category = await Category.find({ name: { $in: category } });
      criteria = {
        ...criteria,
        category: { $in: category.map((categoryId) => categoryId._id) },
      };
    }

    // filter berdasarkan location
    if (location.length) {
      location = await Location.find({ location: { $in: location } });
      criteria = {
        ...criteria,
        location: { $in: location.map((locationId) => locationId._id) },
      };
    }

    let count = await Product.find(criteria).countDocuments();
    let products = await Product.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip))
      .populate("category")
      .populate("location");

    return res.json({ data: products, count });
  } catch (err) {
    next(err);
  }
};

const updateProduct = async (req, res, next) => {
  try {
    let payload = req.body;
    if (payload.category && payload.category.length) {
      let category = await Category.findById(payload.category);
      if (category.length) {
        payload = {
          ...payload,
          category: category.map((categoryId) => categoryId._id),
        };
      }
    }

    if (payload.location && payload.location.length) {
      let location = await Location.findById(payload.location);
      if (location.length) {
        payload = {
          ...payload,
          location: location.map((locationId) => locationId._id),
        };
      }
    }

    if (req.file) {
      let tmp_path = req.file.path;

      let originalExt = req.file.originalname.split(".");

      [req.file.originalname.split(".").length - 1];

      let filename = req.file.filename + "." + originalExt;

      let target_path = path.resolve(
        config.rootPath,
        `public/upload/${filename}`
      );

      const src = fs.createReadStream(tmp_path);

      const dest = fs.createWriteStream(target_path);

      src.pipe(dest);

      src.on("end", async () => {
        let product = await Product.findOne({ _id: req.params.id });

        let currentImage = `${config.rootPath}/public/upload/${product.image_url}`;

        if (fs.existsSync(currentImage)) {
          fs.unlinkSync(currentImage);
        }

        product = await Product.findOneAndUpdate(
          { _id: req.params.id },
          { ...payload, image_url: filename },
          { new: true, runValidators: true }
        );

        return res.json(product);
      });

      src.on("error", async () => {
        next(err);
      });
    } else {
      let product = await Product.findOneAndUpdate(
        { _id: req.params.id },
        payload,
        { new: true, runValidators: true }
      );

      return res.json(product);
    }
  } catch (err) {
    if (err && err.name === "validationError") {
      return res.json({
        error: 1,
        message: err.message,
        fields: err.errors,
      });
    }
    next(err);
  }
};

// menghapus product
const deleteProduct = async (req, res, next) => {
  try {
    let product = await Product.findOneAndDelete({ _id: req.params.id });

    let currentImage = `${config.rootPath}/public/upload/${product.image_url}`;

    if (fs.existsSync(currentImage)) {
      fs.unlinkSync(currentImage);
    }

    return res.json(product);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createProduct,
  updateProduct,
  getListProduct,
  deleteProduct,
};
