const router = require("express").Router();
const multer = require("multer");
const locationController = require("./controller");

router.post("/locations", multer().none(), locationController.createLocation);

router.put(
  "/locations/:id",
  multer().none(),
  locationController.updateLocation
);

router.delete("/locations/:id", locationController.deleteLocation);

router.get("/locations", locationController.getLocation);

module.exports = router;
