const Location = require("./model");

const createLocation = async (req, res, next) => {
  try {
    let payload = req.body;

    let location = new Location(payload);

    await location.save();

    return res.json(location);
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const updateLocation = async (req, res, next) => {
  try {
    let payload = req.body;

    let location = await Location.findOneAndUpdate(
      { _id: req.params.id },
      payload,
      { new: true, runValidators: true }
    );

    return res.json(location);
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const getLocation = async (req, res, next) => {
  try {
    let { limit = 10, skip = 0, search = "" } = req.query;
    let criteria = {};

    // filter berdasarkan query
    if (search.length) {
      criteria = { ...criteria, name: { $regex: `${search}`, $options: "i" } };
    }

    let count = await Location.find(criteria).countDocuments();
    let location = await Location.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip));

    return res.json({ data: location, count });
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

const deleteLocation = async (req, res, next) => {
  try {
    let deleted = await Location.findOneAndDelete({ _id: req.params.id });

    return res.json(deleted);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createLocation,
  updateLocation,
  deleteLocation,
  getLocation,
};
