const router = require("express").Router();

const locationController = require("./controller");

router.get("/locations", locationController.getLocation);

module.exports = router;
