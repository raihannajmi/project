const Location = require("./model");

const getLocation = async (req, res, next) => {
  try {
    let { limit = 10, skip = 0, search = "" } = req.query;
    let criteria = {};

    // filter berdasarkan query
    if (search.length) {
      criteria = { ...criteria, name: { $regex: `${search}`, $options: "i" } };
    }

    let count = await Location.find(criteria).countDocuments();
    let location = await Location.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip));

    return res.json({ data: location, count });
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

module.exports = {
  getLocation,
};
