const router = require("express").Router();

const categoryController = require("./controller");

router.get("/categories", categoryController.getCategory);

module.exports = router;
