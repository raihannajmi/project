const Category = require("./model");

const getCategory = async (req, res, next) => {
  try {
    let { limit = 10, skip = 0, query = "" } = req.query;
    let criteria = {};

    // filter berdasarkan query
    if (query.length) {
      criteria = { ...criteria, name: { $regex: `${query}`, $options: "i" } };
    }

    let count = await Category.find(criteria).countDocuments();
    let category = await Category.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip));

    return res.json({ data: category, count });
  } catch (err) {
    if (err && err.name === "ValidatorError") {
      return res.json({
        error: 1,
        message: err.message,
        field: err.errors,
      });
    }
    next(err);
  }
};

module.exports = {
  getCategory,
};
