const Product = require("./model");
const Category = require("../category/model");
const Location = require("../location/model");

const getListProduct = async (req, res, next) => {
  try {
    let {
      limit = 10,
      skip = 0,
      search = "",
      category = [],
      location = [],
    } = req.query;
    let criteria = {};

    if (search.length) {
      criteria = { ...criteria, name: { $regex: `${search}`, $options: "i" } };
    }

    // filter berdasarkan category
    if (category.length) {
      category = await Category.find({ name: { $in: category } });
      criteria = {
        ...criteria,
        category: { $in: category.map((categoryId) => categoryId._id) },
      };
    }

    if (location.length) {
      location = await Location.find({ location: { $in: location } });
      criteria = {
        ...criteria,
        location: { $in: location.map((locationId) => locationId._id) },
      };
    }

    let count = await Product.find(criteria).countDocuments();
    let products = await Product.find(criteria)
      .limit(parseInt(limit))
      .skip(parseInt(skip))
      .populate("category")
      .populate("location");

    return res.json({ data: products, count });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  getListProduct,
};
