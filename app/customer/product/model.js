const mongoose = require("mongoose");

const { model, Schema } = mongoose;

const productSchema = Schema(
  {
    name: {
      type: String,
      minLength: [3, "Panjang nama makanan minimal 3 karakter"],
      maxLength: [255, "Panjang nama makanan maksimal 255 karakter"],
      required: [true, "Nama produk harus diisi"],
    },

    description: {
      type: String,
      maxLength: [1000, "panjang deskripsi maksimal 1000 karakter"],
    },

    price: {
      type: Number,
      required: [true, "Price must fill"],
    },

    unit: {
      type: Number,
      required: [true, "Unit must fill"],
    },

    regularPrice: {
      type: Number,
      required: [true, "Price must fill"],
    },

    image: String,

    weight: { type: Number, required: true },

    size: { type: Number || null, required: true },

    category: [
      {
        type: Schema.Types.ObjectId,
        ref: "Category",
      },
    ],

    location: [
      {
        type: Schema.Types.ObjectId,
        ref: "Location",
      },
    ],
  },
  { timestamps: true }
);

module.exports = model("productCustomer", productSchema, "products");
