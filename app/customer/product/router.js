const router = require("express").Router();
const multer = require("multer");
const os = require("os");

const productController = require("./controller");

router.get("/products", productController.getListProduct);

module.exports = router;
