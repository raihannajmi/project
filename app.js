var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

const productRouter = require("./app/admin/product/router");
const categoryRouter = require("./app/admin/category/router");
const locationRouter = require("./app/admin/location/router");
const customerProductRouter = require("./app/customer/product/router");
const customerCategoryRouter = require("./app/customer/category/router");
const customerLocationRouter = require("./app/customer/location/router");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/admin", productRouter);
app.use("/admin", categoryRouter);
app.use("/admin", locationRouter);
app.use("/customer", customerProductRouter);
app.use("/customer", customerCategoryRouter);
app.use("/customer", customerLocationRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
